---
edx_platform_repo: "https://vus_https_deploy:{{ COMMON_GITLAB_DEPLOY_TOKEN }}@gitlab.com/vuslms/edx/edx-platform.git"
EDX_PLATFORM_VERSION: "main"

EDXAPP_PLATFORM_NAME: 'VUS'
EDXAPP_LANG: "en_US.UTF-8"
EDXAPP_LANGUAGE_CODE: "en"
EDXAPP_TIME_ZONE: "Asia/Saigon"

EDXAPP_SESSION_COOKIE_DOMAIN: ".vus.edu.vn"
EDXAPP_SITE_NAME: "lms.vus.edu.vn"
EDXAPP_CMS_SITE_NAME: "studio.vus.edu.vn"
EDXAPP_PREVIEW_LMS_BASE: "preview.vus.edu.vn"
EDXAPP_LMS_SITE_NAME: "{{ EDXAPP_SITE_NAME }}"
EDXAPP_LMS_BASE: "{{ EDXAPP_SITE_NAME }}"
EDXAPP_CMS_BASE: "{{ EDXAPP_CMS_SITE_NAME }}"

COMMON_HOSTNAME: ''
RG_ADD_HOSTNAME_TO_HOSTS_FILE: false

EDXAPP_LMS_BASE_SCHEME: "https"
COMMON_LMS_BASE_URL: "{{ EDXAPP_LMS_BASE_SCHEME }}://{{ EDXAPP_LMS_BASE }}"
EDXAPP_LMS_ROOT_URL: "{{ EDXAPP_LMS_BASE_SCHEME }}://{{ EDXAPP_LMS_BASE }}"

EDXAPP_SESSION_COOKIE_NAME: "{{ EDXAPP_LMS_SITE_NAME }}_edxapp"
EDXAPP_CSRF_COOKIE_SECURE: true
EDXAPP_SESSION_COOKIE_SECURE: true

EDXAPP_USE_GIT_IDENTITY: False
edxapp_git_identity: "{{ edxapp_app_dir }}/.ssh/id_rsa"

# Azure storage specifics

EDXAPP_DEFAULT_FILE_STORAGE: 'storages.backends.azure_storage.AzureStorage'
AZURE_ACCOUNT_NAME: !!null
AZURE_ACCOUNT_KEY: !!null
# NOTE!
# before deploying edxapp container must be created (manually or by terraform)
# Public access level: Blob (anonymouns read access for blobs only)
AZURE_CONTAINER: !!null

EDXAPP_GRADE_STORAGE_TYPE_LOCAL: false
EDXAPP_GRADE_STORAGE_TYPE: ''
EDXAPP_GRADE_STORAGE_CLASS: "{{ EDXAPP_DEFAULT_FILE_STORAGE }}"
EDXAPP_GRADE_STORAGE_KWARGS: {}

EDXAPP_PROFILE_IMAGE_BACKEND:
  class: "{{ EDXAPP_DEFAULT_FILE_STORAGE }}"
  options: {}

EDXAPP_VIDEO_IMAGE_SETTINGS:
  VIDEO_IMAGE_MAX_BYTES : 2097152
  VIDEO_IMAGE_MIN_BYTES : 2048
  STORAGE_CLASS: "{{ EDXAPP_DEFAULT_FILE_STORAGE }}"
  STORAGE_KWARGS: {}
  DIRECTORY_PREFIX: 'video-images/'

EDXAPP_VIDEO_TRANSCRIPTS_SETTINGS:
  VIDEO_TRANSCRIPTS_MAX_BYTES : 3145728
  STORAGE_CLASS: "{{ EDXAPP_DEFAULT_FILE_STORAGE }}"
  STORAGE_KWARGS: {}
  DIRECTORY_PREFIX: 'video-transcripts/'

# END Azure storage specifics

EDXAPP_FEATURES_OVERRIDE:
  AUTOMATIC_VERIFY_STUDENT_IDENTITY_FOR_TESTING: false
  RG_DELETE_RETIRED: "{{ COMMON_ENABLE_ACCOUNT_DELETION }}"
  ENABLE_XBLOCK_VIEW_ENDPOINT: true
  ENABLE_RG_INSTRUCTOR_ANALYTICS: "{{ COMMON_RG_INSTRUCTOR_ANALYTICS_ENABLE }}"
  RG_ANALYTICS_GRADE_CRON_MINUTE: '*/15'
  RG_ANALYTICS_GRADE_CRON_HOUR: '*'
  RG_ANALYTICS_GRADE_CRON_DOM: '*'
  RG_ANALYTICS_GRADE_CRON_DOW: '*'
  RG_ANALYTICS_GRADE_CRON_MONTH: '*'
  # RG gamification
  RG_GAMIFICATION:
    ENABLED: "{{ COMMON_ENABLE_GAMMA }}"
    RG_GAMIFICATION_ENDPOINT: "https://{{ GAMMA_SITE_NAME }}"
    SECRET: "{{ RG_GAMIFICATION_SECRET }}"
    KEY: "{{ RG_GAMIFICATION_KEY }}"
    IGNORED_EVENT_TYPES:
      - edx.certificate.created
      - edx.course.student_notes.added
  # RG gamification

EDXAPP_FEATURES: "{{ _EDXAPP_FEATURES | combine(EDXAPP_FEATURES_OVERRIDE, recursive=True) }}"

ELASTICSEARCH_CLUSTERED: False

EDXAPP_LMS_ENV_EXTRA:
  RG_SENTRY_DSN: "{{ COMMON_RG_SENTRY_DSN }}"
  RG_SENTRY_ENVIRONMENT: "{{ project_environment }}"
  THIRD_PARTY_AUTH_BACKENDS:
    - "social_core.backends.google.GoogleOAuth2"
    - "social_core.backends.linkedin.LinkedinOAuth2"
    - "social_core.backends.facebook.FacebookOAuth2"
    - "social_core.backends.azuread.AzureADOAuth2"
    - "third_party_auth.saml.SAMLAuthBackend"
    - "third_party_auth.lti.LTIAuthBackend"
  AZURE_ACCOUNT_NAME: "{{ AZURE_ACCOUNT_NAME }}"
  AZURE_ACCOUNT_KEY: "{{ AZURE_ACCOUNT_KEY }}"
  AZURE_CONTAINER: "{{ AZURE_CONTAINER }}"
  AZURE_CONNECTION_TIMEOUT_SECS: 300
  AZURE_OBJECT_PARAMETERS:
    content_encoding: compress
  ORA2_FILEUPLOAD_BACKEND: django

EDXAPP_CMS_ENV_EXTRA:
  RG_SENTRY_DSN: "{{ COMMON_RG_SENTRY_DSN }}"
  RG_SENTRY_ENVIRONMENT: "{{ project_environment }}"
  COURSE_IMPORT_EXPORT_STORAGE: "{{ EDXAPP_DEFAULT_FILE_STORAGE }}"
  AZURE_ACCOUNT_NAME: "{{ AZURE_ACCOUNT_NAME }}"
  AZURE_ACCOUNT_KEY: "{{ AZURE_ACCOUNT_KEY }}"
  AZURE_CONTAINER: "{{ AZURE_CONTAINER }}"
  AZURE_OVERWRITE_FILES: True
  AZURE_CONNECTION_TIMEOUT_SECS: 300
  AZURE_OBJECT_PARAMETERS:
    content_encoding: compress
  ORA2_FILEUPLOAD_BACKEND: django

CONFIGURE_JWTS: True
EDXAPP_EDX_PLATFORM_REVISION: koa-rg
EDXAPP_PLATFORM_FACEBOOK_ACCOUNT: 'https://www.facebook.com/AnhvanhoiVietMy.VUS'

# rg_analytics
EDXAPP_ADDL_INSTALLED_APPS: ['rg_instructor_analytics', 'rg_instructor_analytics_log_collector']
analytics_log_watcher_sleep_time: 300

EDXAPP_INSTALL_PRIVATE_REQUIREMENTS: True
EDXAPP_PRIVATE_REQUIREMENTS:
  - name: 'git+https://vus_https_deploy:{{ COMMON_GITLAB_DEPLOY_TOKEN }}@gitlab.com/vuslms/edx/instructor-analytics@main#egg=rg_instructor_analytics'
    extra_args: '-e'
  - name: 'git+https://vus_https_deploy:{{ COMMON_GITLAB_DEPLOY_TOKEN }}@gitlab.com/vuslms/edx/instructor-analytics-log-collector@main#egg=rg_instructor_analytics_log_collector'
    extra_args: '-e'
  - name: 'git+https://vus_https_deploy:{{ COMMON_GITLAB_DEPLOY_TOKEN }}@gitlab.com/vuslms/edx/edx-gamma-dashboard@main#egg=edx_gamma_dashboard'
    extra_args: '-e'
  - name: 'git+https://vus_https_deploy:{{ COMMON_GITLAB_DEPLOY_TOKEN }}@gitlab.com/vuslms/edx/edx-gamma-bridge@main#egg=edx_gamma_bridge'
    extra_args: '-e'

EDXAPP_EXTRA_REQUIREMENTS:
  - name: 'azure-common==1.1.27'
  - name: 'azure-storage-blob==2.1.0'
  - name: 'azure-storage-common==2.1.0'

EDXAPP_SUPPORT_SITE_LINK: "{{ EDXAPP_LMS_ROOT_URL }}/support/contact_us"
EDXAPP_ACTIVATION_EMAIL_SUPPORT_LINK: "{{ EDXAPP_SUPPORT_SITE_LINK }}"

EDXAPP_CONTACT_MAILING_ADDRESS: '189 Nguyen Thi Minh Khai Street, Pham Ngu Lao Ward, District 1, Ho Chi Minh City, Vietnam'

EDXAPP_REGISTRATION_EXTRA_FIELDS:
  city: "hidden"
  confirm_email: "hidden"
  country: "optional"
  gender: "optional"
  goals: "optional"
  honor_code: "hidden"
  level_of_education: "optional"
  mailing_address: "hidden"
  terms_of_service: "hidden"
  year_of_birth: "optional"
