
run:
	docker run -it \
		-v $(PWD):/build \
		-e ANSIBLE_SSH_KEY \
		-e ANSIBLE_VAULT_KEY \
                registry.gitlab.com/vuslms/edx/configuration:vus-develop \
		bash -c "cd /build && bash"
