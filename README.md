software versions
=================

- Base OpenEdx release version: [open-release/koa.3](https://github.com/edx/configuration/tree/open-release/koa.3)
- OpenEdx VM Operating System version: `Ubuntu 20.04 (focal)`
- Ansible version: `2.11.1`

configuration
=============

OpenEdx deployment based on https://github.com/edx/configuration.git repository. This repository
contains number of Ansible roles and playbooks used for installation of OS packages, open-source
applications and OpenEdx services and microservices.

VUS configuration repository - https://gitlab.com/vuslms/edx/configuration.git
is the second-level fork of edX configuration repository (fork of RaccoonGang's repository). 
The repository contains changes and modification made to support of VUS multi-instance infrastructure and
configuration of Azure resources. All applied modifications can be inspected by reviewing commits made after 12 May, 2021.

deployment
==========

To configure deployment for specific environment many default variables for Ansible roles in configuration
repository need to be redefined. For this purpose deployment repository is used - 
https://gitlab.com/vuslms/edx/deployment.git

File `hosts.yml` contains ansible inventory (https://docs.ansible.com/ansible/latest/network/getting_started/first_inventory.html).
Directories `group_vars` contain ansible extra-variables for project environments:
`group_vars/vus` - extra-variables common for all environment
`group_vars/vus_prod` - variable overrives for Production environment
`group_vars/vus_uat` - variable overrives for UAT environment, based on `group_vars/vus`
`group_vars/vus_devs` - variable overrives for Development environments, based on `group_vars/vus`
`host_vars/tqm` - variable overrives for Azure TQM environment, based on `group_vars/vus` and `group_vars/vus_devs`
`host_vars/vus_customer_dev` - variable overrives for Azure Development environment, based on `group_vars/vus` and `group_vars/vus_devs`
`host_vars/vus_dev` - variable overrives for RaccoonGang's Development environment, based on `group_vars/vus` and `group_vars/vus_devs`

Ansible configuration stored in `ansible.cfg`. Deployment playbooks contains sequential calls of
ansible tasks and roles from `configuration` repository to deploy OpenEdx services.

dev.yml - RaccoonGang's Development environment tasks
customer-dev.yml - Azure Development environment tasks
tqm.yml - Azure TQM environment tasks
uat.yml - Azure UAT environment tasks
uat_bootstrap.yml - VM bootstrap tasks for UAT environment
uat_services.yml - Services VM specific tasks for UAT environment
prod.yml - Production environment tasks
prod_bootstrap.yml - VM bootstrap tasks for Production environment
prod_services.yml - Services VM specific tasks for Production environment
cosmosdb_init.yml - Tasks for initialization of Azure COSMOS DB for UAT and Production environments

project specific private repositories used in `configuration` tasks
===================================================================

All repositories is in private space and protected from world-wide access.
For deployment purposes project specific `GitLab deployment token` is used to clone repositories to target VMs.
This token is stored in `deployment` repository vault storage files.

- `edx-platform` - code and modules of LMS, CMS, LMS-workers and CMS-workers services - https://gitlab.com/vuslms/edx/edx-platform.git
  configured here - https://gitlab.com/vuslms/edx/deployment/-/blob/vus/group_vars/vus/edxapp.yml#L3

- `edx-theme` - comprehensite theming templates, static source files and static assets overrides (overrides of `edx-platform` repository content)
  configured here - https://gitlab.com/vuslms/edx/deployment/-/blob/vus/group_vars/vus/theme.yml#L2

- `cs_comments_service` - forum service code https://gitlab.com/vuslms/edx/cs_comments_service

- `RaccoonGang Analytics` - https://gitlab.com/rg-developers/instructor-analytics.git
- `RaccoonGang Analytics log collector tool` - https://gitlab.com/rg-developers/instructor-analytics-log-collector.git
- `gammification` - https://gitlab.com/vuslms/edx/rg-gamification/edx-gamma-bridge.git,
                    https://gitlab.com/vuslms/edx/edx-gamma-dashboard.git

vault storage
=============

This ansible extra-variables files contains OpenEdx cretentials and encrypted with `ansible-vault` tool. Encryption password stored here -
https://raccoongang.1password.com/vaults/w67bjxvypmfdifhcbu75m3vumq/allitems/3n36g27bqjick7t5dhictcaipi/

- PROD - https://gitlab.com/vuslms/edx/deployment/-/blob/vus/group_vars/vus/secrets.yml
- UAT - https://gitlab.com/vuslms/edx/deployment/-/blob/vus/group_vars/vus_uat/secrets.yml (overrides of PROD)
- TQM - https://gitlab.com/vuslms/edx/deployment/-/blob/vus/group_vars/vus_devs/secrets.yml (overrides of PROD)
        https://gitlab.com/vuslms/edx/deployment/-/blob/vus/host_vars/vus_tqm/secrets.yml (overrides of `group_vars/vus_devs`)
- DEV - https://gitlab.com/vuslms/edx/deployment/-/blob/vus/group_vars/vus_devs/secrets.yml (overrides of PROD)
        https://gitlab.com/vuslms/edx/deployment/-/blob/vus/host_vars/vus_customer_dev/secrets.yml (overrides of `group_vars/vus_devs`)

how to run Ansible playbooks
============================

Prerequsites: 
- any Azure VM must be directly network accessible and accessible via SSH protocol.
- to achive accessibility goal for Azure VMs in RaccoonGang's CI/CD infrastructure, the local GitLab Runner software was
  installed on Service VMs on Azure DEV, UAT and Production environments (due to limited VMs access via Azure Bastion service only).

How to run ansible provision locally (currently only possible for RaccoonGang's Develpment environment, which is
available via SSH from the Internet):

- install Ansible package on any suitable OS (https://docs.ansible.com/ansible/latest/installation_guide/index.html);
- clone `configuration` repository;
- clone `deployment` repository;
- switch current directory to deployment repository content;
- in `ansible.cfg` change the path to point to directory where `configuration` repository was saved in this settings:
  `roles_path`, `library` and `inventory`;
- verify content of `hosts.yml` to be sure that hosts for all environments configured and reachable via SSH;
- start deployment process by executing this command: `ansible-playbook dev.yml -K -k`.
  `-K` parameter will force Ansible to ask a `sudo` password, `-k` parameter will force Ansible to ask `ssh` password.
  If SSH connection between current host and remote host configured with SSH key-pair then `-k` parameter can be ommited.
  If remote user has passwordless `sudo` configuration then `-K` parameter can be ommited;
- to enable debug logging `-vvv` parameter can be used
