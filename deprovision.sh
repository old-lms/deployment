#!/bin/sh

apt-get clean
rm -rf ~/.cache /root/.cache

echo -n > /var/mail/root
echo -n > /var/log/syslog

rm -rf /tmp/mako_*
rm -f /edx/var/log/*/*.gz /edx/var/log/*/*.log-*
rm -f /var/log/*/*.gz /var/log/*.gz

for i in `find /edx/var/log/ -name "*.log" ` ; do
  echo -n > $i
done
for i in `find /var/log/ -type f` ; do
  echo -n > $i
done

/usr/bin/cloud-init clean --logs
/usr/sbin/waagent -deprovision -force
